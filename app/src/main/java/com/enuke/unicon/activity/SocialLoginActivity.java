package com.enuke.unicon.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.enuke.unicon.R;

public class SocialLoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social_login);
    }

    /***
     *
     * @param v
     * this Handle click for email login section
     */
    public void emaillogin(View v) {
        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
    }

    /***
     *
     * @param v
     * this Handle click for KakaoTalk Login section
     */
    public void kakaoTalkLogin(View v) {
        Toast.makeText(this, "kakaoTalkLogin", Toast.LENGTH_SHORT).show();

    }

    /***
     *
     * @param v
     * this Handle click for Facebook Login section
     */
    public void facebooklogin(View v) {
        Toast.makeText(this, "facebooklogin", Toast.LENGTH_SHORT).show();

    }

    /***
     *
     * @param v
     * this Handle click for Google Login section
     */
    public void googlelogin(View v) {
        Toast.makeText(this, "googlelogin", Toast.LENGTH_SHORT).show();

    }

    /***
     *
     * @param v
     * this Handle click for Client Login section
     */
    public void clientlogin(View v) {
        Toast.makeText(this, "clientlogin", Toast.LENGTH_SHORT).show();

    }

    /***
     *
     * @param v
     * this Handle click for Sign up section
     */
    public void signup(View v) {
        Toast.makeText(this, "signup", Toast.LENGTH_SHORT).show();

    }


}
